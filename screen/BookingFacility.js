import React from 'react';
import { 
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
  Platform,
  Image,
	StatusBar,
  ImageBackground,
  Dimensions,
  
} from 'react-native';

import {
  Container, 
  Content, 
  Header, 
  Left, 
  Right, 
  Icon,
  Button,
  Item, 
  Input, 
  Card, 
  DeckSwiper,
  CardItem,
  Thumbnail,
  Body,
  List,
  ListItem,
} from 'native-base'

import { createStackNavigator } from 'react-navigation';

import facility from '../constants/facility';
import BookingFacility_Form from './BookingFacility_Form';

const { height, width } = Dimensions.get('window');

export class BookingFacility extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return{
      headerTitle : "Booking Facility",
      headerLeft: (
        <Icon 
          name="md-menu" 
          style={{ color: 'black', marginRight: 20, paddingHorizontal: 10 }} 
          onPress={() => navigation.toggleDrawer()} />
      ),
      headerRight: (
        <View style={{ flexDirection: 'row' }}>
          <Icon name="md-qr-scanner" style={{ color: 'black', paddingHorizontal: 10 }} />
          <Icon name="md-qr-scanner" style={{ color: 'black', paddingHorizontal: 10 }} />
        </View>
      )
    }
  }

  render(){
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Content>
          <List>
            <ListItem thumbnail>
              <Left>
                <Thumbnail square large  source={facility[0].image} />
              </Left>
              <Body>
                <Text style={{fontWeight:'bold',fontSize:22}}>{facility[0].location}</Text>
                <Text>1.{facility[0].item1}</Text>
                <Text>2.{facility[0].item2}</Text>
                <Text>3.{facility[0].item3}</Text>
                <Text>...</Text>
              </Body>
              <Right>
                <Button transparent>
                  <Text>View</Text>
                </Button>
              </Right>
            </ListItem>
          </List>

          <List>
            <ListItem thumbnail>
              <Left>
                <Thumbnail square large  source={facility[1].image} />
              </Left>
              <Body>
                <Text style={{fontWeight:'bold',fontSize:22}}>{facility[1].location}</Text>
                <Text>1.{facility[1].item1}</Text>
                <Text>2.{facility[1].item2}</Text>
                <Text>3.{facility[1].item3}</Text>
                <Text>...</Text>
              </Body>
              <Right>
                <Button transparent>
                  <Text>View</Text>
                </Button>
              </Right>
            </ListItem>
          </List>

        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
  },
  androidHeader: {
      ...Platform.select({
          android: {
              paddingTop: StatusBar.currentHeight,
          }
      })
  },
  header: {
    color: 'black', 
    fontSize: 32, 
    paddingTop: 5
  },
  inLine: {
    flexDirection: 'row'
  },
  headerIcon: {
    color: 'black', 
    paddingHorizontal: 10
  },
});

export default createStackNavigator (
  {
    BookingFacility:{
      screen: BookingFacility
    },
    BookingFacility_Form:{
      screen: BookingFacility_Form
    },
},{
      initialRouteName:'BookingFacility'
  }
)