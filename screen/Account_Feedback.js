import React from 'react';
import { 
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
  Platform,
  Image,
	StatusBar,
  ImageBackground,
  Dimensions,
  
} from 'react-native';

import {
  Container, 
  Content, 
  Header, 
  Left, 
  Right, 
	Icon,
	Button,
  Item, 
  Input, 
  Card, 
  CardItem, 
  Label,
  Form,
  Textarea,
  Picker,
} from 'native-base'

import { createStackNavigator } from 'react-navigation'; 

const { height, width } = Dimensions.get('window')

export class Account_Feedback extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      selected2: undefined
    };
  }
  onValueChange2(value: string) {
    this.setState({
      selected2: value
    });
  }

  static navigationOptions = ({ navigation }) => {
    return{
      headerTitle : "Feedback",
      headerLeft: (
        <Icon 
          name="md-menu" 
          style={{ color: 'black', marginRight: 20, paddingHorizontal: 10 }} 
          onPress={() => navigation.toggleDrawer()} />
      ),
      headerRight: (
        <View style={{ flexDirection: 'row' }}>
          <Icon name="md-qr-scanner" style={{ color: 'black', paddingHorizontal: 10 }} />
          <Icon name="md-qr-scanner" style={{ color: 'black', paddingHorizontal: 10 }} />
        </View>
      )
    }
  }

  render(){
    const { navigate } = this.props.navigation;
    return (
      
      <Container>
        <Content>
          <Form>

            <Item floatingLabel>
              <Label>From</Label>
              <Input />
            </Item>

            <Item floatingLabel>
              <Label>Contact Tel</Label>
              <Input />
            </Item>

            <Item floatingLabel>
              <Label>E-mail Address</Label>
              <Input />
            </Item>

            <Item style={{paddingTop:25}}>
              <Label>Department</Label>
              <Right>
                <Picker
                  mode="dropdown"
                  iosIcon={<Icon name="arrow-down" />}
                  placeholder="Select Department"
                  placeholderStyle={{ color: "#bfc6ea" }}
                  selectedValue={this.state.selected2}
                  onValueChange={this.onValueChange2.bind(this)}>
                  <Picker.Item label="Production" value="key0" />
                  <Picker.Item label="Research and Development" value="key1" />
                  <Picker.Item label="Purchasing" value="key2" />
                  <Picker.Item label="Marketing " value="key3" />
                </Picker>
              </Right>
            </Item>

            <Item style={{flexDirection:'column',alignItems:'flex-start',paddingTop:25}}>
              <Label>Message</Label>
              <Textarea rowSpan={2} placeholder="Please enter your message." />
            </Item>

            <View style={{ paddingTop: 15, alignItems: 'center' }}>
              <Button primary style={{ width: '90%', alignItems: 'center', justifyContent: 'center' }} onPress={() => {navigate('Home')}}>
                <Text style={{ color: 'white', fontSize: 20, }}>Submit</Text>
              </Button>
            </View>

          </Form>
        </Content>
      </Container>

    );
  }
}

export default createStackNavigator (
  {
    Account_Feedback:{
      screen: Account_Feedback
    }
},{
      initialRouteName:'Account_Feedback'
  }
)