import React from 'react';
import { 
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
  Platform,
  Image,
	StatusBar,
  ImageBackground,
  Dimensions,
  
} from 'react-native';

import {
  Container, 
  Content, 
  Header, 
  Left, 
  Right, 
  Icon,
  Button,
  Item, 
  Input, 
  Card, 
  DeckSwiper,
  CardItem,
  Thumbnail,
  Body,
  List,
  ListItem,
} from 'native-base'

import { createStackNavigator } from 'react-navigation'; 

import notice from '../constants/notification';
import NotificationScreen from './NotificationScreen';

const { height, width } = Dimensions.get('window');

export class Notification_ListScreen extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return{
      headerTitle : "Notification",
      headerLeft: (
        <Icon 
          name="md-menu" 
          style={{ color: 'black', marginRight: 20, paddingHorizontal: 10 }} 
          onPress={() => navigation.toggleDrawer()} />
      ),
      headerRight: (
        <View style={{ flexDirection: 'row' }}>
          <Icon name="md-qr-scanner" style={{ color: 'black', paddingHorizontal: 10 }} />
          <Icon name="md-qr-scanner" style={{ color: 'black', paddingHorizontal: 10 }} />
        </View>
      )
    }
  }

  render(){
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Content>
          
          <Card>

            <CardItem bordered>
              <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                <Left>
                  <Text style={{ fontSize: 20 }}>Is Coming</Text>
                </Left>
              </View>
            </CardItem>

            <CardItem bordered button onPress={() => {navigate('NotificationScreen')}}>
              <Left>
                <Thumbnail square source={notice[0].image} />
              
              <View style={{flexDirection:'column',paddingHorizontal:10}}>
                <Text style={{fontWeight:'bold',fontSize:22}}>{notice[0].title}</Text>
                <Text>Start Date: {notice[0].start_datetime}</Text>
                <Text>End Date: 	&nbsp;{notice[0].end_datetime}</Text>
              </View>
              </Left>
              <Right>
                  <Text style={{color:'blue'}}>View</Text>
              </Right>
            </CardItem>

          </Card>
          <Card>

            <CardItem bordered >
              <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                <Left>
                  <Text style={{ fontSize: 20 }}>Pass</Text>
                </Left>               
              </View>
            </CardItem>

            <CardItem bordered button onPress={() => {navigate('NotificationScreen')}}>
              <Left>
                <Thumbnail square source={notice[0].image} />
              
              <View style={{flexDirection:'column',paddingHorizontal:10}}>
                <Text style={{fontWeight:'bold',fontSize:22}}>{notice[1].title}</Text>
                <Text>Start Date: {notice[1].start_datetime}</Text>
                <Text>End Date: 	&nbsp;{notice[1].end_datetime}</Text>
              </View>
              </Left>
              <Right>
                  <Text style={{color:'blue'}}>View</Text>
              </Right>
            </CardItem>

            <CardItem bordered button onPress={() => {navigate('NotificationScreen')}}>
              <Left>
                <Thumbnail square source={notice[0].image} />
              
              <View style={{flexDirection:'column',paddingHorizontal:10}}>
                <Text style={{fontWeight:'bold',fontSize:22}}>{notice[2].title}</Text>
                <Text>Start Date: {notice[2].start_datetime}</Text>
                <Text>End Date: 	&nbsp;{notice[2].end_datetime}</Text>
              </View>
              </Left>
              <Right>
                  <Text style={{color:'blue'}}>View</Text>
              </Right>
            </CardItem>

          </Card>

        </Content>
      </Container>

    );
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
  },
  androidHeader: {
      ...Platform.select({
          android: {
              paddingTop: StatusBar.currentHeight,
          }
      })
  },
  header: {
    color: 'black', 
    fontSize: 32, 
    paddingTop: 5
  },
  inLine: {
    flexDirection: 'row'
  },
  headerIcon: {
    color: 'black', 
    paddingHorizontal: 10
  },
});

export default createStackNavigator (
  {
    Notification_ListScreen:{
      screen: Notification_ListScreen,
    },
    NotificationScreen:{
      screen: NotificationScreen,
    },

},{
      initialRouteName:'Notification_ListScreen'
  }
)