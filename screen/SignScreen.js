import React from 'react';
import { 
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
  Platform,
  Image,
	StatusBar,
  ImageBackground,
  Dimensions,
  
} from 'react-native';

import {
  Container, 
  Content, 
  Header, 
  Left, 
  Right, 
	Icon,
	Button,
  Item, 
  Input, 
  Card, 
  CardItem 
} from 'native-base'

import { createStackNavigator } from 'react-navigation'; 

import ForgetPassword from './Sign_ForgetScreen';
import Register from './Sign_RegisterScreen';

const { height, width } = Dimensions.get('window')


export class SignScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loginID: '',
      password: '',
    };
  }

  static navigationOptions = ({ navigation }) => {
    return{
      headerTitle : "Login",
      headerLeft: (
        <Icon 
          name="md-menu" 
          style={{ color: 'black', marginRight: 20, paddingHorizontal: 10 }} 
          onPress={() => navigation.toggleDrawer()} />
      ),
      headerRight: (
        <View style={{ flexDirection: 'row' }}>
          <Icon name="md-qr-scanner" style={{ color: 'black', paddingHorizontal: 10 }} />
          <Icon name="md-qr-scanner" style={{ color: 'black', paddingHorizontal: 10 }} />
        </View>
      )
    }
  }

  render(){
    const { navigate } = this.props.navigation;
    return (
      
      <Content>

        <View style={{ alignItems: 'center', paddingTop: height/5-60 }}>
          <Text style={{ fontWeight: 'bold', fontSize: '60'}}>
						Welcome	
					</Text>
					<Text style={{ fontWeight: 'bold', fontSize: '60'}}>
						New - Estate	
					</Text>
        </View>

				<View style={{paddingTop: height/5-60 , alignItems: 'center'}}>
					<Item regular style={{ width: '90%', }}>
						<Input placeholder='Username' onChangeText={ (loginID) => this.setState({loginID}) }/>
					</Item>
				</View>

				<View  style={{ paddingTop: 15, alignItems: 'center' }}>
					<Item regular style={{ width: '90%', }}>
						<Input placeholder='Password' secureTextEntry={true} />
					</Item>
				</View>
				
				<View style={{ paddingTop: 15, alignItems: 'center' }}>
					<Button primary style={{ width: '90%', alignItems: 'center', justifyContent: 'center' }} onPress={this.login}>
						<Text style={{ color: 'white', fontSize: 20, }}> Login </Text>
					</Button>
				</View>

        <View style={{ paddingTop: 15, alignItems: 'center' }}>
					<Button bordered info style={{ width: '90%', alignItems: 'center', justifyContent: 'center' }} onPress={() => {navigate('ForgetPassword')}}>
						<Text style={{ color: 'skyblue', fontSize: 20, }}> Forget Password </Text>
					</Button>
				</View>

        <View style={{ paddingTop: 15, alignItems: 'center' }}>
					<Button warning style={{ width: '90%', alignItems: 'center', justifyContent: 'center' }} onPress={() => {navigate('Register')}}>
						<Text style={{ color: 'white', fontSize: 20, }}> Register </Text>
					</Button>
				</View>

      </Content>

    );
  }

  login = () => {

    alert(this.state.loginID);

    // fetch('http://127.0.0.1:8000/login', {
    //   method: 'POST',
    //   headers: {
    //     'Accept': 'application/json',
    //     'Content-Type': 'application/json',
    //   },
    //   body: JSON.stringify({
    //     loginID: this.state.loginID,
    //     password: this.state.password,
    //   })
    // })

    // .then((response) => response.json())
    // .then((res) => {
    //   if (res.success === true) {
    //     AsyncStorage.setItem('loginID', res.user);
    //     alert(res.success);
    //   } else {
    //     alert(res.message);
    //   }
    // })
    // .done();

  }
}

const styles = StyleSheet.create ({
  header: {
    color: 'black', 
    fontSize: 32, 
    paddingTop: 5
  },
  inLine: {
    flexDirection: 'row'
  },
  headerIcon: {
    color: 'black', 
    paddingHorizontal: 10
  },
});

export default createStackNavigator ({
  Sign:{
    screen: SignScreen
  },
  ForgetPassword:{
    screen: ForgetPassword
  },
  Register:{
    screen: Register
  }
},{
    initialRouteName:'Sign'
});