import React from 'react';

import { 
  Text, 
  View,
  Dimensions,
  Image,
  StyleSheet 
} from 'react-native';

import {
  Container, 
  Content, 
  Header, 
  Left,
  Title,
  Right, 
  Icon, 
  Item, 
  Input, 
  Card, 
  CardItem 
} from 'native-base'

import { createStackNavigator } from 'react-navigation';

import { Constants, MapView, Location, Permissions } from 'expo';

const { height, width } = Dimensions.get('window')

export class ContactUS extends React.Component {

  state = {
    mapRegion: {
      latitude: 22.3934, 
      longitude: 113.9665,
      latitudeDelta: 0.0922, 
      longitudeDelta: 0.0200
    },
    place: {
      address: '18 Tsing Wun Road, Tuen Mun, New Territories',
      content: '2463 0066',
    },
  };

  static navigationOptions = ({ navigation }) => {
    return{
      headerTitle : "Content US",
      headerLeft: (
        <Icon 
          name="md-menu" 
          style={{ color: 'black', marginRight: 20, paddingHorizontal: 10 }} 
          onPress={() => navigation.toggleDrawer()} />
      ),
    }
  }

  render(){
    // const { navigate } = this.props.navigation;
    return (

        // I hope place a google map on the page with a map but fail. i have comfirm that the code is completely right, as for i run correctly on snake.expo.
        // <MapView
        //   style={{ height: 400 }}
        //   region={{ 
        //     latitude: 22.3934, 
        //     longitude: 113.9665,
        //     latitudeDelta: 0.0922, 
        //     longitudeDelta: 0.0200
        //   }}
        //   onRegionChange={this._handleMapRegionChange}>

        // <MapView.Marker
        //   coordinate={{ 
        //     latitude: this.state.mapRegion.latitude, 
        //     longitude: this.state.mapRegion.longitude, 
        //     latitudeDelta: this.state.mapRegion.latitudeDelta, 
        //     longitudeDelta: this.state.mapRegion.longitudeDelta
        //   }}
        //   />
        // </MapView> 
        
      <Container>
        <Content>
          <Image
            style={{ flex: 1, height: height/5*3, width: '100%', resizeMode: 'contain' }}
            source={require('../assets/icon.png')} />

          <Content style={{ paddingHorizontal: 30 }}>
            <Card>
              <CardItem>
                <View style={{ flexDirection: 'column' }}>
                  <Text style={{ fontWeight: 'bold', fontSize: 20 }}>Address:</Text>
                  <Text style={{ color: 'grey', fontSize: 18 }}>{this.state.place.address}</Text>
                </View>
              </CardItem>
              <CardItem>            
              <View style={{ flexDirection: 'column' }}>
                <Text style={{ fontWeight: 'bold', fontSize: 20 }}>Content:</Text>
                <Text style={{ color: 'grey', fontSize: 18 }}>{this.state.place.content}</Text>
              </View>
            </CardItem>
          </Card>
          </Content>
        </Content>
      </Container>
    );
  }
}

export default createStackNavigator (
  {
    ContactUS:{
      screen: ContactUS
    },
},{
    initialRouteName:'ContactUS'
  }
)