import React from 'react';
import { 
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
  Platform,
  Image,
	StatusBar,
  ImageBackground,
  Dimensions,
  
} from 'react-native';

import {
  Container, 
  Content, 
  Header, 
  Left, 
  Right, 
  Icon,
  Button,
  Item, 
  Input, 
  Card, 
  CardItem 
} from 'native-base'

const { height, width } = Dimensions.get('window')

export default class Sign_RegisterScreen extends React.Component {

  constructor(props){
    super(props);
  }

  static navigationOptions = ({ navigation }) => {
    return{
      headerTitle : "AccountRegister",
    }
  }

  render(){
    const { navigate } = this.props.navigation;
    return (
      <Content>

      <View style={{ alignItems: 'center', paddingTop: height/5-60 }}>
        <Text style={{ fontWeight: 'bold', fontSize: '60'}}>
          New - Estate	
        </Text>
      </View>

      <View style={{paddingTop: height/5-60 , alignItems: 'center' }}>
        <Item regular style={{ width: '90%',  }}>
          <Input placeholder='Username'/>
        </Item>
      </View>

      <View  style={{ paddingTop: 15,alignItems: 'center' }}>
        <Item regular style={{ width: '90%',  }}>
          <Input placeholder='Password' secureTextEntry={true} />
        </Item>
      </View>

      <View  style={{ paddingTop: 15, alignItems: 'center' }}>
        <Item regular style={{ width: '90%',  }}>
          <Input placeholder='Re-type password' secureTextEntry={true} />
        </Item>
      </View>
      
      <View style={{ paddingTop: 15, alignItems: 'center' }}>
        <Button primary style={{ width: '90%', alignItems: 'center', justifyContent: 'center' }} onPress={() => {navigate('HomeScreen')}}>
          <Text style={{ color: 'white', fontSize: 20, }}> Submit </Text>
        </Button>
      </View>

    </Content>

    );
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
  },
  androidHeader: {
      ...Platform.select({
          android: {
              paddingTop: StatusBar.currentHeight,
          }
      })
  },
  header: {
    color: 'black', 
    fontSize: 32, 
    paddingTop: 5
  },
  inLine: {
    flexDirection: 'row'
  },
  headerIcon: {
    color: 'black', 
    paddingHorizontal: 10
  },
});