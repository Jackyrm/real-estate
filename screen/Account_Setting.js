import React from 'react';

import { Container, Header, Content, Button, ListItem, Text, Icon, Left, Body, Right, Switch } from 'native-base';

export default class Account_Setting extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return{
      headerTitle : "Language",
    }
  }

  render() {
    const { goBack } = this.props.navigation;
    return (
      <Container>
        <Content>
          <ListItem icon onPress={() => {goBack(null)}}>
            <Left>
              <Button style={{ backgroundColor: "red" }}>
                <Icon active name="ios-swap" />
              </Button>
            </Left>
            <Body>
              <Text>中文</Text>
            </Body>
            <Right>
              <Icon active name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem icon onPress={() => {goBack(null)}}>
            <Left>
              <Button style={{ backgroundColor: "blue" }}>
                <Icon active name="ios-swap" />
              </Button>
            </Left>
            <Body>
              <Text>English</Text>
            </Body>
            <Right>
              <Icon active name="arrow-forward" />
            </Right>
          </ListItem>

        </Content>
      </Container>
    );
  }
}