import React from 'react';
import { 
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
  Platform,
  Image,
  StatusBar,
  ImageBackground,
  Dimensions,
  
} from 'react-native';

import {
  Container, 
  Content, 
  Header, 
  Left, 
  Right, 
  Icon,
  Button,
  Item, 
  Input, 
  Card, 
  CardItem 
} from 'native-base'

import { createStackNavigator } from 'react-navigation'; 

import accountData from '../constants/account';

import Account_Edit from './Account_Edit';
import Account_Setting from './Account_Setting';

const { height, width } = Dimensions.get('window');
const screen = {
  item1: 'Edit Profile',
  item2: 'Language Setting',
  item3: 'Sign out',
}

export class AccountScreen extends React.Component {

  constructor(props){
    super(props);
  }

  static navigationOptions = ({ navigation }) => {
    return{
      headerTitle : "Account",
      headerLeft: (
        <Icon 
          name="md-menu" 
          style={{ color: 'black', marginRight: 20, paddingHorizontal: 10 }} 
          onPress={() => navigation.toggleDrawer()} />
      ),
      headerRight: (
        <View style={{ flexDirection: 'row' }}>
          <Icon name="md-qr-scanner" style={{ color: 'black', paddingHorizontal: 10 }} />
          <Icon name="md-qr-scanner" style={{ color: 'black', paddingHorizontal: 10 }} />
        </View>
      )
    }
  }
  
  render(){
    const { navigate } = this.props.navigation;
    return (
      <Container>      

        <Content >
          <View style={{ paddingHorizontal: width/2-60, paddingTop: height/5-60 }}>
            <Image source={ require( '../assets/icon.png' )} style={{ height: 120, width: 120, borderRadius: 120/2 }} />
          </View>
          <Text style={{ fontSize: 32, fontWeight: 'bold', textAlign: 'center', paddingTop: 10 }}>
            {accountData.name}
          </Text>

          <Content contentContainerStyle={{ alignItems: 'center', paddingTop: 40  }}>
            <Card style={{ alignItems: 'center' }}>

              <CardItem bordered button onPress={() => {navigate('Account_Edit')}}>
                <Icon active name="ios-contact" />
                  <Text>
                    {screen.item1}
                  </Text>
                <Right>
                  <Icon name="arrow-forward" />
                </Right>
              </CardItem>

              <CardItem bordered button onPress={() => {navigate('Account_Setting')}}>
                <Icon active name="ios-contact" />
                  <Text>
                  {screen.item2}
                  </Text>
                <Right>
                  <Icon name="arrow-forward" />
                </Right>
              </CardItem>

              <CardItem bordered button onPress={() => {navigate('Sign')}}>
                <Icon active name="ios-log-out"/>
                  <Text>
                  {screen.item3}
                  </Text>
                <Right>
                  <Icon name="arrow-forward" />
                </Right>
              </CardItem>

            </Card>
          </Content>
       </Content>

     </Container>

    );
  }
}

export default createStackNavigator (
  {
    AccountScreen:{
      screen: AccountScreen,
    },
    Account_Edit:{
      screen: Account_Edit,
    },
    Account_Setting:{
      screen: Account_Setting,
    }
},{
      initialRouteName:'AccountScreen'
  }
)