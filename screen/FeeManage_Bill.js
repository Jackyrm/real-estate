import React from 'react';
import { 
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
  Platform,
  Image,
	StatusBar,
  ImageBackground,
  Dimensions,
  
} from 'react-native';

import {
  Container, 
  Content, 
  Header, 
  Left, 
  Right, 
	Icon,
	Button,
  Item, 
  Input, 
  Card, 
  CardItem 
} from 'native-base'

import { createStackNavigator } from 'react-navigation'; 

const { height, width } = Dimensions.get('window')
const bill = {
  item: 'Water Bill',
  date: '19/08/2019',
  fee: '300.00',
}

export default class FeeManage_Bill extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return{
      headerTitle : "Bill",
      // headerLeft: (
      //   <Icon 
      //     name="md-menu" 
      //     style={{ color: 'black', marginRight: 20, paddingHorizontal: 10 }} 
      //     onPress={() => navigation.toggleDrawer()} />
      // ),
    }
  }

  render(){
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Content>

          <View style={{ alignItems: 'center', paddingTop: height/8-60 }}>
            <Text style={{ fontWeight: 'bold', fontSize: '60', alignItems: 'center' }}>
              Bill
            </Text>
          </View>

          <View style={{ paddingTop: height/8-60 , alignItems: 'center', paddingHorizontal: 20 }}>

            <Card>
              <CardItem style={{ paddingTop: 50 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>

                  <Left>
                    <Text style={{ fontWeight: 'bold', fontSize: 34 }}>Bill item:</Text>
                  </Left>
                  <Right>
                    <Text style={{ fontSize: 34 }}>{bill.item}</Text>
                  </Right>
                  
                </View>
              </CardItem>

              <CardItem  style={{ paddingTop: 20 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>

                  <Left>
                    <Text style={{ fontWeight: 'bold', fontSize: 34 }}>Bill Date:</Text>
                  </Left>
                  <Right>
                      <Text style={{ fontSize: 34 }}>{bill.date}</Text>
                  </Right>
                  
                </View>
              </CardItem>

              <CardItem  style={{ paddingTop: 20, paddingBottom: 50 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>

                  <Left>
                    <Text style={{ fontWeight: 'bold', fontSize: 34 }}>Bill Fee:</Text>
                  </Left>
                  <Right>
                    <Text style={{ fontSize: 34 }}>$ {bill.fee}</Text>
                  </Right>
                  
                </View>
              </CardItem>

            
            </Card>

          </View>

          <View style={{paddingTop: 20, alignItems: 'center'}}>

            <Button primary style={{ width: '60%', alignItems: 'center', justifyContent: 'center',  }} onPress={() => {navigate('Home')}}>
              <Text style={{ color: 'white', fontSize: 20, }}> Start Pay </Text>
            </Button>

          </View>

        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
  },
  androidHeader: {
      ...Platform.select({
          android: {
              paddingTop: StatusBar.currentHeight,
          }
      })
  },
  header: {
    color: 'black', 
    fontSize: 32, 
    paddingTop: 5
  },
  inLine: {
    flexDirection: 'row'
  },
  headerIcon: {
    color: 'black', 
    paddingHorizontal: 10
  },
});

// export default createStackNavigator (
//   {
//     Bill:{
//       screen: FeeManage_Bill
//     }
// },{
//       initialRouteName:'FeeManage_Bill'
//   }
// )