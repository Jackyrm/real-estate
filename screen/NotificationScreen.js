import React from 'react';
import { 
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
  Platform,
  Image,
	StatusBar,
  ImageBackground,
  Dimensions,
  
} from 'react-native';

import {
  Container, 
  Content, 
  Header, 
  Left, 
  Right, 
  Icon,
  Button,
  Item, 
  Input, 
  Card, 
  DeckSwiper,
  CardItem,
  Thumbnail,
  Body,
  Title,
} from 'native-base'

import notice from '../constants/notification';

const { height, width } = Dimensions.get('window');


export default class Notification extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return{
      headerTitle : "Booking Facility",
    }
  }

  render(){
    const { navigate } = this.props.navigation;
    return (
     
      <Container>
        <View>
          <DeckSwiper
            dataSource={notice}
            renderItem={item =>
              <Card >
                {/* style={{ elevation: 1 }} */}
                <CardItem bordered>
                  <Left>
                    <Thumbnail source={item.image} />
                    <Body>
                      <Text style={{fontSize:22}}>{item.creator}</Text>
                      <Text style={{fontSize:18,color:'grey'}}>{item.postdate}</Text>
                    </Body>
                  </Left>
                </CardItem>

                <CardItem style={{paddingTop:10}}>
                  <Image style={{ height: 350, flex: 1 }} source={item.image} />
                </CardItem>
                <CardItem style={{flexDirection:'column',alignItems:'flex-start'}}>
                  <Text style={{fontWeight:'bold',fontSize:32}}>{item.title}</Text>
                  <View style={{flexDirection:'row', paddingTop:10}}>
                    <Text style={{paddingEnd:50,fontSize:18}}>Reason: </Text>
                    <Text style={{fontSize:18}}>{item.happened}</Text>
                  </View>
                  <View style={{flexDirection:'row', paddingTop:10}}>
                    <Text style={{paddingEnd:5,fontSize:18}}>Affected Place:</Text>
                    <Text style={{fontSize:18}}>{item.notice_location} - {item.location_detail}</Text>
                  </View>
                  <View style={{flexDirection:'row', paddingTop:10}}>
                    <Text style={{paddingEnd:72,fontSize:18}}>Date:</Text>
                    <Text style={{fontSize:18}}>{item.start_datetime} - {item.end_datetime}</Text>
                  </View>
                </CardItem>
              </Card>
            }
          />
        </View>
      </Container>

    );
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
  },
  androidHeader: {
      ...Platform.select({
          android: {
              paddingTop: StatusBar.currentHeight,
          }
      })
  },
  header: {
    color: 'black', 
    fontSize: 32, 
    paddingTop: 5
  },
  inLine: {
    flexDirection: 'row'
  },
  headerIcon: {
    color: 'black', 
    paddingHorizontal: 10
  },
});