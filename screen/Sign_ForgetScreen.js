import React from 'react';
import { 
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
  Platform,
  Image,
	StatusBar,
  ImageBackground,
  Dimensions,
  
} from 'react-native';

import {
  Container, 
  Content, 
  Header, 
  Left, 
  Right, 
	Icon,
	Button,
  Item, 
  Input, 
  Card, 
  CardItem 
} from 'native-base'

const { height, width } = Dimensions.get('window')

export default class Sign_ForgetScreen extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return{
      headerTitle : "Forget Password",
    }
  }
  
  render(){
    const { navigate } = this.props.navigation;
    return (
      <Container>

        {/* <Header style={ styles.androidHeader }>
          <Left>
            <Icon name="md-menu" style={ styles.headerIcon } onPress={() => navigation.toggleDrawer()} />
          </Left>
          <Text style={ styles.header }>Forget Password</Text>
          <Right style={ styles.inLine }>
            <Icon name="md-qr-scanner" style={ styles.headerIcon } />
            <Icon name="md-qr-scanner" style={ styles.headerIcon } />
          </Right>
       </Header> */}

        <Content>

          <View style={{ alignItems: 'center', paddingTop: height/5-60 }}>
            <Text style={{ fontWeight: 'bold', fontSize: '60', alignItems: 'center' }}>
              Forget - Password	
            </Text>
          </View>

          <View style={{paddingTop: height/5-60 , width: '100%', alignItems: 'center' }}>
            <Item regular style={{ width: '90%', alignItems: 'center'  }}>
              <Input placeholder='Phone number'/>
            </Item>
          </View>

          
          <View style={{ paddingTop: 15, alignItems: 'center' }}>
            <Button primary style={{ width: '90%', alignItems: 'center', justifyContent: 'center' }} onPress={() => {navigate('AccountSignScreen')}}>
              <Text style={{ color: 'white', fontSize: 20, }}> Submit </Text>
            </Button>
          </View>

        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
  },
  androidHeader: {
      ...Platform.select({
          android: {
              paddingTop: StatusBar.currentHeight,
          }
      })
  },
  header: {
    color: 'black', 
    fontSize: 32, 
    paddingTop: 5
  },
  inLine: {
    flexDirection: 'row'
  },
  headerIcon: {
    color: 'black', 
    paddingHorizontal: 10
  },
});