import React from 'react';
import { 
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
  Platform,
  Image,
  StatusBar,
  Dimensions,
  
} from 'react-native';

import {
  Container, 
  Content, 
  Header, 
  Left, 
  Right, 
  Icon, 
  Button,
  Item, 
  Input, 
  Card, 
  CardItem,
} from 'native-base';

import Swiper from 'react-native-swiper';

import { createStackNavigator } from 'react-navigation';
import account from '../constants/account';
import facility from '../constants/facility';
import bill from '../constants/bill';
import notification from '../constants/notification';

const { height, width } = Dimensions.get('window');

export class HomeScreen extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return{
      headerTitle : "Home",
      headerLeft: (
        <Icon 
          name="md-menu" 
          style={{ color: 'black', marginRight: 20, paddingHorizontal: 10 }} 
          onPress={() => navigation.toggleDrawer()} />
      ),
      headerRight: (
        <View style={{ flexDirection: 'row' }}>
          <Icon name="md-qr-scanner" style={{ color: 'black', paddingHorizontal: 10 }} />
          <Icon name="md-qr-scanner" style={{ color: 'black', paddingHorizontal: 10 }} />
        </View>
      )
    }
  }

  render(){
    const { navigate } = this.props.navigation;
    return (

      <Container>
        
       <Content style={{ backgroundColor: 'white', marginTop: 0 }}>
        <View style={{
          height: 50, backgroundColor: 'white', 
          flexDirection: 'row', paddingHorizontal: 5, alignItems: 'center', justifyContent: 'space-between' 
          }}>
          <Text style={{ fontSize: 28 }}>Hello, {account.name}</Text>
          <View style={{ flexDirection: 'row' }}>
            <Icon name="sunny"  />
            <Text style={{ fontSize: 28 }}>33ºC </Text>
          </View>
        </View>

        <Card style={{ marginLeft: 5, marginRight: 5, paddingBottom: 5 }}>
          <CardItem bordered>
            <Left>
              <Text style={{ fontSize: 25,paddingHorizontal:10, }}>New Notification</Text>
            </Left>
            <Right>
              <Button transparent onPress={() => {navigate('Notification_ListScreen')}}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={{ fontSize: 20 }}>View More&nbsp;</Text>
                  <Icon name="arrow-forward" style={{ fontSize: 20 }} />
                </View>
              </Button>
            </Right>
          </CardItem>
          <CardItem>

          <Swiper
          autoplay={true}
          style={{ height: 350, }}>
            <View style={{ flex: 1 }}>
              <Image
                style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                source={notification[0].image} />
            </View>
            <View style={{ flex: 1 }}>
              <Image
                style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                source={notification[1].image} />
            </View>
            <View style={{ flex: 1 }}>
              <Image
                style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                source={notification[2].image} />
            </View>
          </Swiper>

          </CardItem>
        </Card>
        
        <Card style={{ marginLeft: 5, marginRight: 5, paddingBottom: 5 }}>

          <CardItem bordered>
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
              <Left>
                <Text style={{ fontSize: 25,paddingHorizontal:10, }}>Facility</Text>
              </Left>
              <Right>
                <Button transparent onPress={() => {navigate('BookingFacility')}}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontSize: 20 }}>View More&nbsp;</Text>
                    <Icon name="arrow-forward" style={{ fontSize: 20 }} />
                  </View>
                </Button>
              </Right>
            </View>
          </CardItem>

          <CardItem>
            <View>
              <Image style={{ height: width/3-20, width:  width/3-20 }}
                source={facility[0].image}/>
            </View>
            <Right style={{ flex: 1, alignItems: 'flex-start', height: 90, paddingHorizontal: 20 }}>
              <Text style={{ fontWeight:'bold',fontSize:20 }}>{facility[0].location}</Text>
              <Text style={{ color:'grey',fontSize:18, }}>{facility[0].item1}</Text>
              <Text style={{ color:'grey',fontSize:18, }}>{facility[0].item2}</Text>
              <Text style={{ color:'grey',fontSize:18, }}>{facility[0].item3}</Text>
            </Right>
          </CardItem>

          <CardItem>
            <View>
              <Image style={{ height: width/3-20, width:  width/3-20 }}
                source={facility[1].image}/>
            </View>
            <Right style={{ flex: 1, alignItems: 'flex-start', height: 90, paddingHorizontal: 20 }}>
              <Text style={{ fontWeight:'bold',fontSize:20 }}>{facility[1].location}</Text>
              <Text style={{ color:'grey',fontSize:18, }}>{facility[1].item1}</Text>
              <Text style={{ color:'grey',fontSize:18, }}>{facility[1].item2}</Text>
              <Text style={{ color:'grey',fontSize:18, }}>{facility[1].item3}</Text>
            </Right>
          </CardItem>

        </Card> 

        <Card style={{ marginLeft: 5, marginRight: 5, paddingBottom: 5 }}>

          <CardItem bordered>
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
              <Left>
                <Text style={{ fontSize: 25,paddingHorizontal:10, }}>You have a new bill</Text>
              </Left>
              <Right>
                <Button transparent onPress={() => {navigate('Fee')}}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontSize: 20 }}>View More&nbsp;</Text>
                    <Icon name="arrow-forward" style={{ fontSize: 20 }} />
                  </View>
                </Button>
              </Right>
            </View>
          </CardItem>

          <CardItem>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 1, alignItems: 'flex-start', height: 50 }}>
                <Text style={{ fontWeight:'bold',fontSize:20}}>{bill[0].item}</Text>
                <Text style={{ color:'grey',fontSize:18}}>Exp Date: {bill[0].exp}</Text>
              </View>
              <View>
                <Image style={{ height: width/5-20, width:  width/5-20 }} source={bill[0].image}/>
              </View>
            </View>
          </CardItem>

          <CardItem>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 1, alignItems: 'flex-start', height: 50 }}>
                <Text style={{ fontWeight:'bold',fontSize:20}}>{bill[1].item}</Text>
                <Text style={{ color:'grey',fontSize:18}}>Exp Date: {bill[1].exp}</Text>
              </View>
              <View>
                <Image style={{ height: width/5-20, width:  width/5-20 }} source={bill[1].image}/>
              </View>
            </View>
          </CardItem>

          <CardItem>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 1, alignItems: 'flex-start', height: 50 }}>
                <Text style={{ fontWeight:'bold',fontSize:20}}>{bill[2].item}</Text>
                <Text style={{ color:'grey',fontSize:18}}>Exp Date: {bill[2].exp}</Text>
              </View>
              <View>
                <Image style={{ height: width/5-20, width:  width/5-20 }} source={bill[2].image}/>
              </View>
            </View>
          </CardItem>

        </Card> 
      </Content>

      </Container>

    );
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
  },
  androidHeader: {
      ...Platform.select({
          android: {
              paddingTop: StatusBar.currentHeight,
          }
      })
  },
  header: {
    color: 'black', 
    fontSize: 32, 
    paddingTop: 5
  },
  inLine: {
    flexDirection: 'row'
  },
  headerIcon: {
    color: 'black', 
    paddingHorizontal: 10
  },
});

export default createStackNavigator (
  {
    Home:{
      screen: HomeScreen
    }
  },{
      initialRouteName:'Home',
  }
)

{/* a type of desige
<Card>
<CardItem bordered>
  <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>

    <Left>
      <Text style={{ fontSize: 20 }}>Activity</Text>
    </Left>
    <Right>
      <View style={{ flexDirection: 'row' }}>
        <Text style={{ fontSize: 20 }}>View More&nbsp;</Text>
        <Icon name="arrow-forward" style={{ fontSize: 20 }} />
      </View>
    </Right>

  </View>
</CardItem>

<View><Text style={{ fontSize: 10 }}>	&nbsp;</Text></View>
<View style={{ flexDirection: 'row', alignItems: 'center',justifyContent: 'space-around' }}>
  <View>
    <Image style={{ height: width/2-20, width: width/2-20 }} source={require("../assets/icon.png")}/>
    <Text style={{ fontSize: 20, textAlign: 'center' }}>sdafsd</Text>
  </View>
  <View>
    <Image style={{ height: width/2-20, width: width/2-20 }} source={require("../assets/icon.png")}/>
    <Text style={{ fontSize: 20, textAlign: 'center' }}>sdafsd</Text>
  </View>
</View>
<View><Text style={{ fontSize: 30 }}>	&nbsp;</Text></View>
<View style={{ flexDirection: 'row', alignItems: 'center',justifyContent: 'space-around' }}>
  <View>
    <Image style={{ height: width/2-20, width: width/2-20 }} source={require("../assets/icon.png")}/>
    <Text style={{ fontSize: 20, textAlign: 'center' }}>sdafsd</Text>
  </View>
  <View>
    <Image style={{ height: width/2-20, width: width/2-20 }} source={require("../assets/icon.png")}/>
    <Text style={{ fontSize: 20, textAlign: 'center' }}>sdafsd</Text>
  </View>
</View>
<View><Text style={{ fontSize: 30 }}>	&nbsp;</Text></View>
</Card> */}