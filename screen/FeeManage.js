import React from 'react';
import { 
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
  Platform,
  Image,
	StatusBar,
  ImageBackground,
  Dimensions,
  
} from 'react-native';

import {
  Container, 
  Content, 
  Header, 
  Left, 
  Right, 
	Icon,
	Button,
  Item, 
  Input, 
  Card, 
  CardItem 
} from 'native-base'

import { createStackNavigator } from 'react-navigation'; 
import FeeManage_Bill from './FeeManage_Bill';

const { height, width } = Dimensions.get('window');
import bill from '../constants/bill';

export class FeeManage extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return{
      headerTitle : "Fee",
      headerLeft: (
        <Icon 
          name="md-menu" 
          style={{ color: 'black', marginRight: 20, paddingHorizontal: 10 }} 
          onPress={() => navigation.toggleDrawer()} />
      ),
      headerRight: (
        <View style={{ flexDirection: 'row' }}>
          <Icon name="md-qr-scanner" style={{ color: 'black', paddingHorizontal: 10 }} />
          <Icon name="md-qr-scanner" style={{ color: 'black', paddingHorizontal: 10 }} />
        </View>
      )
    }
  }
  
  render(){
    const { navigate } = this.props.navigation;
    return (
      <Container>

        <Content>

          <View style={{ alignItems: 'center', paddingTop: height/8-60 }}>
            <Text style={{ fontWeight: 'bold', fontSize: '60', alignItems: 'center' }}>
              Fee Management
            </Text>
          </View>

          <View style={{ paddingTop: height/8-60 , alignItems: 'center' }}>

            <Card>
              <CardItem bordered>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>

                  <Left>
                    <Text style={{ fontSize: 20 }}>Is Coming</Text>
                  </Left>
                  <Right>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={{ fontSize: 20 }}>View More&nbsp;</Text>
                      <Icon name="arrow-forward" style={{ fontSize: 20 }} />
                    </View>
                  </Right>
                  
                </View>
              </CardItem>
              
              <CardItem button onPress={() => {navigate('Bill')}}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flex: 1, alignItems: 'flex-start', height: 50 }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 20}}>{bill[0].item}</Text>
                    <Text style={{ color: 'grey', fontSize: 18}}>Exp Date: {bill[0].date}</Text>
                  </View>
                  <View>
                    <Image style={{ height: width/5-20, width:  width/5-20 }} source={require("../assets/icon.png")}/>
                  </View>
                </View>
              </CardItem>
            </Card>

          </View>
				  
          <View style={{ alignItems: 'center' }}>
            <Card>

              <CardItem bordered>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>

                  <Left>
                    <Text style={{ fontSize: 20 }}>Paid</Text>
                  </Left>
                  <Right>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={{ fontSize: 20 }}>View More&nbsp;</Text>
                      <Icon name="arrow-forward" />
                    </View>
                  </Right>
                  
                </View>
              </CardItem>
              
              <CardItem>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flex: 1, alignItems: 'flex-start', height: 50 }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 20}}>{bill[1].item}</Text>
                    <Text style={{ color: 'grey', fontSize: 18}}>Exp Date: {bill[1].date}</Text>
                  </View>
                  <View>
                    <Image style={{ height: width/5-20, width:  width/5-20 }} source={require("../assets/icon.png")}/>
                  </View>
                </View>
              </CardItem>

              <CardItem>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flex: 1, alignItems: 'flex-start', height: 50 }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 20}}>{bill[2].item}</Text>
                    <Text style={{ color: 'grey', fontSize: 18}}>Exp Date: {bill[2].date}</Text>
                  </View>
                  <View>
                    <Image style={{ height: width/5-20, width:  width/5-20 }} source={require("../assets/icon.png")}/>
                  </View>
                </View>
              </CardItem>

            </Card>
          </View>
         

          {/* navigate('ChatScreen', { userName: 'Lucy' }); */}



        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  
});

export default createStackNavigator (
  {
    FeeManage:{
      screen: FeeManage
    },
    Bill:{
      screen: FeeManage_Bill
    }
},{
      initialRouteName:'FeeManage'
  }
)