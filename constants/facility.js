export default 
[
  {
    location: 'Gym Room',
    item1: 'Stationary Bikes',
    item2: 'Aerobic steppers',
    item3: 'Cable Pulley Machines',
    state: 'available',
    image: require('../assets/icon.png'),
  },
  {
    location: 'Sport Facility',
    item1: 'Baskball Room',
    item2: 'Swimming pool',
    item3: 'Tennis Room',
    state: 'available',
    image: require('../assets/icon.png'),
  },
]