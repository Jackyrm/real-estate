export default 
[
  {
    item: 'Water Bill',
    date: '19/08/2019',
    exp: '29/08/2019',
    fee: '300.00',
    image: require('../assets/icon.png'),
  },
  {
    item: 'Electricity bill',
    date: '10/08/2019',
    exp: '21/08/2019',
    fee: '890.00',
    image: require('../assets/icon.png'),
  },
  {
    item: 'Rent',
    date: '01/08/2019',
    exp: '11/08/2019',
    fee: '30300.00',
    image: require('../assets/icon.png'),
  },
]