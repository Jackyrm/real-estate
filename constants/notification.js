export default 
[
  {
    title: 'Maintenance Elevator',
    postdate: '10/8/2019',
    creator: 'Property Management Limited',
    notice_location: 'Lee Shing House',
    location_detail: 'Elevator B',
    start_datetime: '20/8/2019 12:00',
    end_datetime: '20/8/2019 17:00',
    happened: 'Replacement parts',
    image: require('../assets/icon.png'),
  },
  {
    title: 'Maintenance Electric Generator',
    postdate: '12/8/2019',
    creator: 'Property Management Limited',
    notice_location: 'Lee Shing House',
    location_detail: 'Whole block',
    start_datetime: '23/8/2019 12:00',
    end_datetime: '23/8/2019 17:00',
    happened: 'Replacement parts',
    image: require('../assets/icon.png'),
  },
  {
    title: 'Suspension of Fresh Water Supply',
    postdate: '12/8/2019',
    creator: 'Property Management Limited',
    notice_location: 'Lee Shing House',
    location_detail: 'Whole block',
    start_datetime: '23/8/2019 09:00',
    end_datetime: '23/8/2019 17:00',
    happened: 'Relocate the Fresh Water Pipe',
    image: require('../assets/icon.png'),
  }
]
