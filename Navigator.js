import React from 'react';
import { 
  View,
  Text,
  Image,
  Dimensions,
} from 'react-native';

import {
  Icon,
} from 'native-base'

import {createDrawerNavigator, createAppContainer, createStackNavigator, DrawerItems, SafeAreaView} from 'react-navigation';

import HomeScreen from './screen/HomeScreen';
import SignScreen from './screen/SignScreen';
import AccountScreen from './screen/AccountScreen';
import FeeManage from './screen/FeeManage';
import ContactUS from './screen/ContactUS';
import Feedback from './screen/Account_Feedback';
// import BookingFacility_Form from './screen/BookingFacility_Form';
import BookingFacility from './screen/BookingFacility';

import Notification from './screen/NotificationScreen';
import Notification_ListScreen from './screen/Notification_ListScreen';

import { ScrollView } from 'react-native-gesture-handler';
import account from './constants/account';

const WIDTH = Dimensions.get('window').width;

const CustomDrawer = (props) => (
  <SafeAreaView>
    <View style={{paddingTop:30,backgroundColor:'grey',paddingBottom:30}}>
      <View style={{flexDirection:'column'}}>
        <View style={{flexDirection:'row', justifyContent:'center', }}>
          <Image style={{ width: 50, height : 50 }} source={require('./assets/icon.png')} />

          <Text style={{alignSelf:'center',color:'white',fontSize:32}}>&nbsp;&nbsp;Hello~ {account.name}</Text>
        </View>
          <Text style={{color:'white',paddingTop:10,alignSelf:'center',fontSize:22}}>Welcome to New - Estate</Text>
        </View>
    
    </View>
    <ScrollView>
      <DrawerItems {...props}/>
    </ScrollView>
  </SafeAreaView>
)

const AppNavigator = createDrawerNavigator(
	{
    Sign:{
      screen: SignScreen,
      navigationOptions: {
        drawerIcon: ({tintColor}) => (
          <Icon name="home" style={{color:tintColor}}/>
        )
      },
    },
    Home:{
      screen: HomeScreen,
      navigationOptions: {
        drawerIcon: ({tintColor}) => (
          <Icon name="home" style={{color:tintColor}}/>
        )
      },
    },
    Account: {
      screen: AccountScreen,
      navigationOptions: {
        drawerIcon: ({tintColor}) => (
          <Icon name="home" style={{color:tintColor}}/>
        )
      },
    },
    Fee: {
      screen: FeeManage,
      navigationOptions: {
        drawerIcon: ({tintColor}) => (
          <Icon name="home" style={{color:tintColor}}/>
        )
      },
    },
    BookingFacility: { 
      screen: BookingFacility,
      navigationOptions: {
        drawerIcon: ({tintColor}) => (
          <Icon name="home" style={{color:tintColor}}/>
        )
      },
    },
    ContactUS: {
      screen: ContactUS,
      navigationOptions: {
        drawerIcon: ({tintColor}) => (
          <Icon name="home" style={{color:tintColor}}/>
        )
      },
    },
    Feedback: {
      screen: Feedback,
      navigationOptions: {
        drawerIcon: ({tintColor}) => (
          <Icon name="home" style={{color:tintColor}}/>
        )
      },
    },
    // BookingFacility_Form:BookingFacility_Form,
    Notification: {
      screen: Notification_ListScreen,
      navigationOptions: {
        drawerIcon: ({tintColor}) => (
          <Icon name="home" style={{color:tintColor}}/>
        )
      },
    },
	},
	{
    initialRouteName: 'Sign',
		drawerType: 'back',
    drawerWidth: WIDTH*0.7,
    contentComponent: CustomDrawer,
  }
);

const AppContainer = createAppContainer(AppNavigator);
export default AppContainer;